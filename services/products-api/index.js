const { RoboHydraHead } = require('robohydra').heads

module.exports = {
  getBodyParts (conf, modules) {
    const { fixtures } = modules

    return {
      heads: [
        new RoboHydraHead({
          name: 'products-api',
          path: 'products.json',
          handler (req, res) {
            const category = req.queryParams.category;
            var response = fixtures.load(category + '.json');

            res.statusCode = 200;
            res.send(response);
          }
        })
      ]
    }
  }
}
