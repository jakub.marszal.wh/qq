const { RoboHydraHead } = require('robohydra').heads

module.exports = {
  getBodyParts (conf, modules) {
    const { fixtures } = modules;

        return {
            heads: [
              new RoboHydraHead({
                  name: 'test',
                  path: 'test.json',
                  handler (req, res) {
                    const response = fixtures.load('data.json');
                    res.statusCode = 200;
                    res.headers['content-type'] = 'application/json;';
                    res.send(response);
                  }
                }),
                new RoboHydraHead({
                  name: 'test-delay',
                  path: 'test-delay.json',
                  handler (req, res) {
                    const response = fixtures.load('data-delay.json')
                    res.statusCode = 200
                    setTimeout(function () {
                      res.send(response)
                    }, 1500)
                  }
                }),
                new RoboHydraHead({
                  name: 'test-delay-query',
                  path: 'test-delay-query.json',
                  handler (req, res) {
                    res.statusCode = 200
                    setTimeout(function () {
                      res.send('Loading after: ' + req.queryParams.timeout)
                    }, req.queryParams.timeout || 1000)
                  }
                }),
                new RoboHydraHead({
                  name: 'test-delay-post',
                  path: 'test-delay-post.json',
                  handler (req, res) {
                    const timeout = req.body['timeout']
                    res.statusCode = 200
                    setTimeout(function () {
                      res.send('Loading after: ' + timeout)
                    }, timeout || 1000)
                  }
                })
            ]
        }
    }
}
