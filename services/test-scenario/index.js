const { RoboHydraHead } = require('robohydra').heads

module.exports = {
  getBodyParts (conf, modules) {
    const { fixtures } = modules;

    const firstScenario = {
      heads: [
        new RoboHydraHead({
          name: 'scenario-first',
          path: 'scenario',
          handler (req, res) {
            const response = fixtures.load('data-scenario-1.json')
            res.statusCode = 200
            res.headers['content-type'] = 'application/json;'
            res.send(response)
          }
        })
      ]
    }

    const secondScenario = {
      heads: [
        new RoboHydraHead({
          name: 'scenario-second',
          path: 'scenario',
          handler (req, res) {
            const response = fixtures.load('data-scenario-2.json')
            res.statusCode = 200
            res.headers['content-type'] = 'application/json;'
            res.send(response)
          }
        })
      ]
    }

    const failureScenario = {
      heads: [
        new RoboHydraHead({
          name: 'scenario-fail',
          path: 'scenario',
          handler (req, res) {
            res.statusCode = 500
            res.send('(Synthetic) Internal Server Error')
          }
        })
      ]
    }

    return {
      heads: firstScenario.heads,
      scenarios: {
        scenario1: firstScenario,
        scenario2: secondScenario,
        scenario_error_500: failureScenario
      }
    }
  }
}
